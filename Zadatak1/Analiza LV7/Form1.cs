﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Analiza_LV7
{
    public partial class Form1 : Form
    {
        bool xTurn = true;
        Graphics g; List<Pen> pens = new List<Pen>();
        bool[] full = { false, false, false, false, false, false, false, false, false };
        int[] igra = { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        int score1 = 0, score2 = 0;
        public Form1()
        {
            InitializeComponent();

        }

        private void buttonNew_Click(object sender, EventArgs e)
        {
            for(int i=0; i<9; i++)
            {
                full[i] = false;
            }
            pictureBox1.Image = null;
            pictureBox2.Image = null;
            pictureBox3.Image = null;
            pictureBox4.Image = null;
            pictureBox5.Image = null;
            pictureBox6.Image = null;
            pictureBox7.Image = null;
            pictureBox8.Image = null;
            pictureBox9.Image = null;
            for(int i = 0; i< 9; i++)
            {
                igra[i] = 0;
            }
            score1 = 0;
            score2 = 0;
            labelScoreO.Text = score1.ToString();
            xTurn = true;
            labelScoreX.Text = score1.ToString();
            textBoxPlayerX.Text = "Igrač 1";
            textBoxPlayerO.Text = "Igrač 2";
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if (xTurn == true && full[0] == false)
            {
                g = pictureBox1.CreateGraphics();
                g.DrawString("x", new Font("Arial", 60), new SolidBrush(Color.Black), 0, 0);
                full[0] = true;
                xTurn = !xTurn;
                igra[0] = 1;
            }
            else if(xTurn == false && full[0] == false)
            {
                g = pictureBox1.CreateGraphics();
                g.DrawString("o", new Font("Arial", 60), new SolidBrush(Color.Black), 0, 0);
                full[0] = true;
                xTurn = !xTurn;
                igra[0] = 2;
            }
            if(provjera() == 3)
            {
                MessageBox.Show("TIE!");
                clear();
            }
            if(provjera() == 1)
            {
                MessageBox.Show(textBoxPlayerX.Text + " POBJEDUJE!");
                score1++;
                labelScoreX.Text = score1.ToString();
                clear();
            }
            if (provjera() == 2)
            {
                MessageBox.Show(textBoxPlayerO.Text +" POBJEDUJE!");
                score2++;
                labelScoreO.Text = score2.ToString();
                clear();
            }
            changeTrn();

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            if (xTurn == true && full[1] == false)
            {
                g = pictureBox2.CreateGraphics();
                g.DrawString("x", new Font("Arial", 60), new SolidBrush(Color.Black), 0, 0);
                full[1] = true;
                xTurn = !xTurn;
                igra[1] = 1;
            }
            else if (xTurn == false && full[1] == false)
            {
                g = pictureBox2.CreateGraphics();
                g.DrawString("o", new Font("Arial", 60), new SolidBrush(Color.Black), 0, 0);
                full[1] = true;
                xTurn = !xTurn;
                igra[1] = 2;
            }
            if (provjera() == 3)
            {
                MessageBox.Show("TIE!");
                clear();
            }
            if (provjera() == 1)
            {
                MessageBox.Show(textBoxPlayerX.Text + " POBJEDUJE!");
                score1++;
                labelScoreX.Text = score1.ToString();
                clear();
            }
            if (provjera() == 2)
            {
                MessageBox.Show(textBoxPlayerO.Text + " POBJEDUJE!");
                score2++;
                labelScoreO.Text = score2.ToString();
                clear();
            }
            changeTrn();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            if (xTurn == true && full[2] == false)
            {
                g = pictureBox3.CreateGraphics();
                g.DrawString("x", new Font("Arial", 60), new SolidBrush(Color.Black), 0, 0);
                full[2] = true;
                xTurn = !xTurn;
                igra[2] = 1;
            }
            else if (xTurn == false && full[2] == false)
            {
                g = pictureBox3.CreateGraphics();
                g.DrawString("o", new Font("Arial", 60), new SolidBrush(Color.Black), 0, 0);
                full[2] = true;
                xTurn = !xTurn;
                igra[2] = 2;
            }
            if (provjera() == 3)
            {
                MessageBox.Show("TIE!");
                clear();
            }
            if (provjera() == 1)
            {
                MessageBox.Show(textBoxPlayerX.Text + " POBJEDUJE!");
                score1++;
                labelScoreX.Text = score1.ToString();
                clear();
            }
            if (provjera() == 2)
            {
                MessageBox.Show(textBoxPlayerO.Text + " POBJEDUJE!");
                score2++;
                labelScoreO.Text = score2.ToString();
                clear();
            }
            changeTrn();
        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            if (xTurn == true && full[3] == false)
            {
                g = pictureBox6.CreateGraphics();
                g.DrawString("x", new Font("Arial", 60), new SolidBrush(Color.Black), 0, 0);
                full[3] = true;
                xTurn = !xTurn;
                igra[3] = 1;
            }
            else if (xTurn == false && full[3] == false)
            {
                g = pictureBox6.CreateGraphics();
                g.DrawString("o", new Font("Arial", 60), new SolidBrush(Color.Black), 0, 0);
                full[3] = true;
                xTurn = !xTurn;
                igra[3] = 2;
            }
            if (provjera() == 3)
            {
                MessageBox.Show("TIE!");
                clear();
            }
            if (provjera() == 1)
            {
                MessageBox.Show(textBoxPlayerX.Text + " POBJEDUJE!");
                score1++;
                labelScoreX.Text = score1.ToString();
                clear();
            }
            if (provjera() == 2)
            {
                MessageBox.Show(textBoxPlayerO.Text + " POBJEDUJE!");
                score2++;
                labelScoreO.Text = score2.ToString();
                clear();
            }
            changeTrn();
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            if (xTurn == true && full[4] == false)
            {
                g = pictureBox5.CreateGraphics();
                g.DrawString("x", new Font("Arial", 60), new SolidBrush(Color.Black), 0, 0);
                full[4] = true;
                xTurn = !xTurn;
                igra[4] = 1;
            }
            else if (xTurn == false && full[4] == false)
            {
                g = pictureBox5.CreateGraphics();
                g.DrawString("o", new Font("Arial", 60), new SolidBrush(Color.Black), 0, 0);
                full[4] = true;
                xTurn = !xTurn;
                igra[4] = 2;
            }
            if (provjera() == 3)
            {
                MessageBox.Show("TIE!");
                clear();
            }
            if (provjera() == 1)
            {
                MessageBox.Show(textBoxPlayerX.Text + " POBJEDUJE!");
                score1++;
                labelScoreX.Text = score1.ToString();
                clear();
            }
            if (provjera() == 2)
            {
                MessageBox.Show(textBoxPlayerO.Text + " POBJEDUJE!");
                score2++;
                labelScoreO.Text = score2.ToString();
                clear();
            }
            changeTrn();
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            if (xTurn == true && full[5] == false)
            {
                g = pictureBox4.CreateGraphics();
                g.DrawString("x", new Font("Arial", 60), new SolidBrush(Color.Black), 0, 0);
                full[5] = true;
                xTurn = !xTurn;
                igra[5] = 1;
            }
            else if (xTurn == false && full[5] == false)
            {
                g = pictureBox4.CreateGraphics();
                g.DrawString("o", new Font("Arial", 60), new SolidBrush(Color.Black), 0, 0);
                full[5] = true;
                xTurn = !xTurn;
                igra[5] = 2;
            }
            if (provjera() == 3)
            {
                MessageBox.Show("TIE!");
                clear();
            }
            if (provjera() == 1)
            {
                MessageBox.Show(textBoxPlayerX.Text + " POBJEDUJE!");
                score1++;
                labelScoreX.Text = score1.ToString();
                clear();
            }
            if (provjera() == 2)
            {
                MessageBox.Show(textBoxPlayerO.Text + " POBJEDUJE!");
                score2++;
                labelScoreO.Text = score2.ToString();
                clear();
            }
            changeTrn();
        }

        private void pictureBox9_Click(object sender, EventArgs e)
        {
            if (xTurn == true && full[6] == false)
            {
                g = pictureBox9.CreateGraphics();
                g.DrawString("x", new Font("Arial", 60), new SolidBrush(Color.Black), 0, 0);
                full[6] = true;
                xTurn = !xTurn;
                igra[6] = 1;
            }
            else if (xTurn == false && full[6] == false)
            {
                g = pictureBox9.CreateGraphics();
                g.DrawString("o", new Font("Arial", 60), new SolidBrush(Color.Black), 0, 0);
                full[6] = true;
                xTurn = !xTurn;
                igra[6] = 2;
            }
            if (provjera() == 3)
            {
                MessageBox.Show("TIE!");
                clear();
            }
            if (provjera() == 1)
            {
                MessageBox.Show(textBoxPlayerX.Text + " POBJEDUJE!");
                score1++;
                labelScoreX.Text = score1.ToString();
                clear();
            }
            if (provjera() == 2)
            {
                MessageBox.Show(textBoxPlayerO.Text + " POBJEDUJE!");
                score2++;
                labelScoreO.Text = score2.ToString();
                clear();
            }
            changeTrn();
        }

        private void pictureBox8_Click(object sender, EventArgs e)
        {
            if (xTurn == true && full[7] == false)
            {
                g = pictureBox8.CreateGraphics();
                g.DrawString("x", new Font("Arial", 60), new SolidBrush(Color.Black), 0, 0);
                full[7] = true;
                xTurn = !xTurn;
                igra[7] = 1;
            }
            else if (xTurn == false && full[7] == false)
            {
                g = pictureBox8.CreateGraphics();
                g.DrawString("o", new Font("Arial", 60), new SolidBrush(Color.Black), 0, 0);
                full[7] = true;
                xTurn = !xTurn;
                igra[7] = 2;
            }
            if (provjera() == 3)
            {
                MessageBox.Show("TIE!");
                clear();
            }
            if (provjera() == 1)
            {
                MessageBox.Show(textBoxPlayerX.Text + " POBJEDUJE!");
                score1++;
                labelScoreX.Text = score1.ToString();
                clear();
            }
            if (provjera() == 2)
            {
                MessageBox.Show(textBoxPlayerO.Text + " POBJEDUJE!");
                score2++;
                labelScoreO.Text = score2.ToString();
                clear();
            }
            changeTrn();
        }

        private void pictureBox7_Click(object sender, EventArgs e)
        {
            if (xTurn == true && full[8] == false)
            {
                g = pictureBox7.CreateGraphics();
                g.DrawString("x", new Font("Arial", 60), new SolidBrush(Color.Black), 0, 0);
                full[8] = true;
                xTurn = !xTurn;
                igra[8] = 1;
            }
            else if (xTurn == false && full[8] == false)
            {
                g = pictureBox7.CreateGraphics();
                g.DrawString("o", new Font("Arial", 60), new SolidBrush(Color.Black), 0, 0);
                full[8] = true;
                xTurn = !xTurn;
                igra[8] = 2;
            }
            if (provjera() == 3)
            {
                MessageBox.Show("TIE!");
                clear();
            }
            if (provjera() == 1)
            {
                MessageBox.Show(textBoxPlayerX.Text + " POBJEDUJE!");
                score1++;
                labelScoreX.Text = score1.ToString();
                clear();
            }
            if (provjera() == 2)
            {
                MessageBox.Show(textBoxPlayerO.Text + " POBJEDUJE!");
                score2++;
                labelScoreO.Text = score2.ToString();
                clear();
            }
            changeTrn();
        }
        
        private int provjera()
        {
            if((igra[0]==igra[1] && igra[0]==igra[2] && igra[0] == 1)|| (igra[3] == igra[4] && igra[5] == igra[3] && igra[3] == 1) || (igra[6] == igra[7] && igra[7] == igra[8] && igra[8] == 1) || (igra[0] == igra[3] && igra[6] == igra[3] && igra[3] == 1) || (igra[1] == igra[4] && igra[7] == igra[1] && igra[1] == 1) || (igra[2] == igra[5] && igra[5] == igra[8] && igra[8] == 1) || (igra[0] == igra[4] && igra[8] == igra[4] && igra[0] == 1) || (igra[2] == igra[4] && igra[6] == igra[2] && igra[2] == 1))
            {
                return 1;
            }
            if ((igra[0] == igra[1] && igra[0] == igra[2] && igra[0] == 2) || (igra[3] == igra[4] && igra[5] == igra[3] && igra[3] == 2) || (igra[6] == igra[7] && igra[7] == igra[8] && igra[8] == 2) || (igra[0] == igra[3] && igra[6] == igra[3] && igra[3] == 2) || (igra[1] == igra[4] && igra[7] == igra[1] && igra[1] == 2) || (igra[2] == igra[5] && igra[5] == igra[8] && igra[8] == 2) || (igra[0] == igra[4] && igra[8] == igra[4] && igra[0] == 2) || (igra[2] == igra[4] && igra[6] == igra[2] && igra[2] == 2))
            {
                return 2;
            }
            if (igra[0] != 0 && igra[1] != 0 && igra[2] != 0 && igra[3] != 0 && igra[4] != 0 && igra[5] != 0 && igra[6] != 0 && igra[7] != 0 && igra[8] != 0) {
                return 3;
            }
            return 0;
        }

        private void changeTrn()
        {
            if (xTurn == true)
            {
                label5.Text = "X";
            }
            else
            {
                label5.Text = "O";
            }
        }

        private void clear()
        {
            for (int i = 0; i < 9; i++)
            {
                full[i] = false;
            }
            pictureBox1.Image = null;
            pictureBox2.Image = null;
            pictureBox3.Image = null;
            pictureBox4.Image = null;
            pictureBox5.Image = null;
            pictureBox6.Image = null;
            pictureBox7.Image = null;
            pictureBox8.Image = null;
            pictureBox9.Image = null;

            for (int i = 0; i < 9; i++)
            {
                igra[i] = 0;
            }
            xTurn = true;
        }

    }
}
